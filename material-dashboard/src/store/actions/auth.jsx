import axios from '../../axios';

import * as actionType from './actionTypes.jsx';

export const authStart = () => {
    return {
        type: actionType.AUTH_START
    };
};

export const authSuccess = (token, userId) => {
    return {
        type: actionType.AUTH_SUCCESS,
        idToken: token,
        userId: userId
    };
};

export const authFail = (error) => {
    return {
        type: actionType.AUTH_FAIL,
        error: error
    };
};

export const logout = () => {
    console.log("here");
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    return {
        type: actionType.AUTH_LOGOUT
    };
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            console.log('[action/Auth] ======> checkAuthTimeout');
            //dispatch(logout());
        }, expirationTime * 1000);
    };
};

export const auth = (email, password, isSignup) => {
    return  dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password
        };

        //let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyAJIFVxhEciqmbZFm_NdjDBaf3bUqiAHPM';
        let url = 'login';
        
        axios.post(url, authData)
            .then(response => {
                const expirationDate = new Date(new Date().getTime() + 60 * 1000);
                //console.log('response', response);
                localStorage.setItem('token', response.data.success.token);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', response.data.success.userid);
                dispatch(authSuccess(response.data.success.token, response.data.success.userid));
                dispatch(checkAuthTimeout(3600));
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionType.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if(!token) {
            console.log('[action/Auth] ======> authCheckState');
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if(expirationDate <= new Date()) {
                console.log('[action/Auth] ======> date');
                dispatch(logout());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId));
                dispatch(checkAuthTimeout(3600));
                //dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    };
};

export const registerFail = (error) => {
    return {
        type: actionType.REGISTER_FAIL,
        error: error
    };
};
export const register = (accArr) => {
	return dispatch => {
	let url = 'register';
         
    axios.post(url, accArr)
        .then(response => {
        	const expirationDate = new Date(new Date().getTime() + 60 * 1000);
            localStorage.setItem('token', response.data.success.token);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', response.data.success.userid);
            dispatch(authSuccess(response.data.success.token, response.data.success.userid));
            
            dispatch(checkAuthTimeout(60));
        })
        .catch(err => {
        	//console.log('error', err.response);
            dispatch(registerFail(err.response.data.error));
        });
    };
};

/* ========================== START User Start ========================= */
/*export const fetchUserStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START
    };
};*/

export const fetchUserSuccess = (user) => {
    return {
        type: actionType.FETCH_USER_SUCCESS,
        user: user
    };
};

/*export const fetchUserFail = (error) => {
    return {
        type: actionTypes.FETCH_ORDERS_FAIL,
        error: error
    };
};*/

export const fetchUser = (token, userId) => {
    return dispatch => {
        //dispatch(fetchUserStart());

        const userId = localStorage.getItem('userId');
        const token = localStorage.getItem('token');
        const Data = {
            userId : userId
        }; 
        let url = 'get-details';
            
        axios.post(url,Data,{headers: {
            Authorization: 'Bearer '+ token
          }})
        .then(res => {
        	console.log('user fetch', res);
        	dispatch(fetchUserSuccess(res.data.success));
        })
        .catch(err => {
        	console.log('user fetch', err);
            //dispatch(fetchUserFail(err));
        });
    }
}
/* ========================== END User Start ========================= */