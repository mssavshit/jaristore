export {
    auth,
    logout,
    setAuthRedirectPath,
    authCheckState,
    register,
    fetchUser
} from './auth.jsx';