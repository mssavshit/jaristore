import LoginTab from '../components/Auth/Auth.jsx';

const authRoutes = [
    { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default authRoutes;