@extends('layouts.app')

@section('content')
    <h1>Create Post</h1>
    {!! Form::open(['action' => 'AddressController@store', 'methos' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    	<div class="form-group">
    		{{Form::label('address', 'Address')}}
    		{{Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'Address'])}}
    	</div>
    	<div class="form-group">
    		{{Form::label('city', 'City')}}
    		{{Form::text('city', '', ['id' => 'city', 'class' => 'form-control', 'placeholder' => 'City'])}}
    	</div>
    	<div class="form-group">
    		{{Form::label('state', 'State')}}
    		{{Form::text('state', '', ['id' => 'state', 'class' => 'form-control', 'placeholder' => 'State'])}}
    	</div>
    	<div class="form-group">
    		{{Form::label('postal_code', 'Postal Code')}}
    		{{Form::text('postal_code', '', ['id' => 'postal_code', 'class' => 'form-control', 'placeholder' => 'Postal Code'])}}
    	</div>
    	<div class="form-group">
    		{{Form::label('country', 'Country')}}
    		{{Form::text('country', '', ['id' => 'country', 'class' => 'form-control', 'placeholder' => 'Country'])}}
    	</div>
    	{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
	{!! Form::close() !!}
@endsection