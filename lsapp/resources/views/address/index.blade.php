@extends('layouts.app')

@section('content')
    <h1>User Addresses</h1>
    <a href="/addresses/create" class="btn btn-primary">Add New User Address</a>
    @if(count($address) > 0)
        @foreach ($address as $addr)
            <div class="well">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<strong>Address :</strong> {{$addr->address}}, {{$addr->city}}, {{$addr->state}}, {{$addr->country}}-{{$addr->postal_code}}
					</div>
				</div>
            </div>
        @endforeach
        {{ $address->links() }}
    @else
        <p>No adddress found</p>
    @endif
@endsection