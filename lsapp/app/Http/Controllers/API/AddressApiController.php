<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Address;
use Illuminate\Support\Facades\Auth;
use Validator;

class AddressApiController extends Controller
{
    public $successStatus = 200;

    /*public function login(Request $request)
    {
    	if(Auth::attempt(['email' => request('email'), 'password'=>request('password')]))
    	{
    		$user = Auth::user();
    		$success['token'] = $user->createToken('MyApp')->accessToken;
    		$success['userid'] = $user->id;
    		return response()->json(['success'=>$success], $this->successStatus);
    	}
    	else
    	{
    		return response()->json(['error'=>'Unauthorized'], 401);
    	}
    }*/

    public function addAddress(Request $request)
    {
    	$validator = Validator::make($request->all(),[
    		'address' => 'required',
        	'city' => 'required',
        	'state' => 'required',
        	'postal_code' => 'required|max:6|min:6',
        	'country' => 'required'
    	]);

    	if($validator->fails())
    	{
    		return response()->json(['error'=>$validator->errors()], 401);
    	}
    	else
    	{
    		/*$input = $request->all();
    		
    		$input['facebook_id'] = $facebook_id;
    		$input['password'] = bcrypt($input['password']);
    		$user = User::create($input);*/
    		$addr = new Address;
	        $addr->address = $request->input('address');
	       	$addr->city = $request->input('city');
	       	$addr->state = $request->input('state');
	       	$addr->country = $request->input('country');
	       	$addr->postal_code = $request->input('postal_code');
	       	$addr->user_id = auth()->user()->id;
	       	$addr->save();

    		$success['success'] = 'Address added successfully';
    		return response()->json(['success'=>$success], $this->successStatus);	
    	}
    }

    public function getAddresses()
    {
    	$user_id = \Request::segment(3);
    	$address = User::find($user_id)->addresses;
    	return response()->json(['success'=>$address], $this->successStatus);
    }

    /*public function editProfile(Request $request)
    {
    	$user = Auth::user();

    	$validator = Validator::make($request->all(),[
    		'firm_name' => 'required|string|max:255',
    		'name' => 'required|string|max:255',
    		'email' => 'required|string|email|max:255|unique:users,id,'.$user->id,
    		'password' => 'required|min:6',
    		'c_password' => 'required||same:password',
    	]);

    	if($validator->fails())
    	{
    		return response()->json(['error'=>$validator->errors()], 401);
    	}
    	else
    	{

	       	$user->name = $request->input('name');
	       	$user->firm_name = $request->input('firm_name');
	       	if($user->email != $request->input('email'))
	        {
	            $user->email = $request->input('email');
	        }

	        if($request->input('password') !='')
			{
				$user->password = bcrypt($request->input('password'));	
			}

			if($user->facebook_id == '')
			{
				$user->facebook_id = '-';	
			}
	       	$user->save();

	       	$success['name'] = $user->name;
    		$success['firm_name'] = $user->firm_name;
    		return response()->json(['success'=>$success], $this->successStatus);
	    }
    }*/
}
