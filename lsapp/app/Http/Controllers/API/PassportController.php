<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class PassportController extends Controller
{
    public $successStatus = 200;

    public function login(Request $request)
    {
    	if(Auth::attempt(['email' => request('email'), 'password'=>request('password')]))
    	{
    		$user = Auth::user();
    		$success['token'] = $user->createToken('MyApp')->accessToken;
    		$success['userid'] = $user->id;
    		return response()->json(['success'=>$success], $this->successStatus);
    	}
    	else
    	{
    		return response()->json(['error'=>'Unauthorized'], 401);
    	}
    }

    public function register(Request $request)
    {
    	$validator = Validator::make($request->all(),[
    		'firm_name' => 'required',
    		'name' => 'required',
    		'email' => 'required|email|unique:users',
    		'password' => 'required',
    		'c_password' => 'required|same:password',
    	]);

    	if($validator->fails())
    	{
    		return response()->json(['error'=>$validator->errors()], 401);
    	}
    	else
    	{
    		$input = $request->all();
    		$facebook_id = isset($input['facebook_id']) ? $input['facebook_id'] : '';

    		$input['facebook_id'] = $facebook_id;
    		$input['password'] = bcrypt($input['password']);
    		$user = User::create($input);
    		$success['token'] = $user->createToken('MyApp')->accessToken;
    		$success['name'] = $user->name;
    		$success['firm_name'] = $user->firm_name;
    		return response()->json(['success'=>$success], $this->successStatus);	
    	}
    }

    public function getDetails()
    {
    	$user = Auth::user();
    	return response()->json(['success'=>$user], $this->successStatus);
    }

    public function editProfile(Request $request)
    {
    	$user = Auth::user();

    	$validator = Validator::make($request->all(),[
    		'firm_name' => 'required|string|max:255',
    		'name' => 'required|string|max:255',
    		'email' => 'required|string|email|max:255|unique:users,id,'.$user->id,
    		'password' => 'required|min:6',
    		'c_password' => 'required||same:password',
    	]);

    	if($validator->fails())
    	{
    		return response()->json(['error'=>$validator->errors()], 401);
    	}
    	else
    	{

	       	$user->name = $request->input('name');
	       	$user->firm_name = $request->input('firm_name');
	       	if($user->email != $request->input('email'))
	        {
	            $user->email = $request->input('email');
	        }

	        if($request->input('password') !='')
			{
				$user->password = bcrypt($request->input('password'));	
			}

			if($user->facebook_id == '')
			{
				$user->facebook_id = '-';	
			}
	       	$user->save();

	       	$success['name'] = $user->name;
    		$success['firm_name'] = $user->firm_name;
    		return response()->json(['success'=>$success], $this->successStatus);
	    }
    }
}
