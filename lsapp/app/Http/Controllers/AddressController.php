<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use DB;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
    	$user_id = auth()->user()->id;
    	$address = Address::where('user_id', $user_id)->paginate(5);
        return view('address.index')->with('address', $address);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        	'address' => 'required',
        	'city' => 'required',
        	'state' => 'required',
        	'postal_code' => 'required|max:6|min:6',
        	'country' => 'required'
        ]);

        $addr = new Address;
        $addr->address = $request->input('address');
       	$addr->city = $request->input('city');
       	$addr->state = $request->input('state');
       	$addr->country = $request->input('country');
       	$addr->postal_code = $request->input('postal_code');
       	$addr->user_id = auth()->user()->id;
       	$addr->save();

       	return redirect('/addresses')->with('success', 'User address added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
