<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
     // Table Name
    protected $table = 'address';

    // Primary Key
    public $primaryKey = 'id';

    //Timestamps
    public $timestamps = true;  

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
